﻿using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(SpriteRenderer))]
public class Ball : MonoBehaviour
{
    public static event Action<float> OnGoalEvent;
    public static event Action OnMetObstacleEvent;

    public float Speed = 5f;

    [SerializeField] GameObject ArrowPrefab;
    [SerializeField] Color ReadyToTapColor;

    LineRenderer arrow;
    Vector2 direction;
    Vector2 startPosition;
    
    Rigidbody2D _rigidbody;
    Transform _transform;
    SpriteRenderer _sprite;
    CircleCollider2D _collider;

    void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        _transform = GetComponent<Transform>();
        _sprite = GetComponent<SpriteRenderer>();
        _collider = GetComponent<CircleCollider2D>();
    }

    void Start()
    {
        startPosition = _transform.position;
        SimplePool.Preload(ArrowPrefab, 1);
        _sprite.color = ReadyToTapColor;

        MouseController.OnMouseStartedEvent += StartArrow;
        MouseController.OnMouseEndedEvent += EndArrow;
        MouseController.OnMouseDraggingEvent += UpdateArrow;
        TouchController.OnTouchStartedEvent += StartArrow;
        TouchController.OnTouchEndedEvent += EndArrow;
        TouchController.OnTouchDraggingEvent += UpdateArrow;
    }

    void OnDestroy()
    {
        UnSubscribe();
    }

    void UnSubscribe()
    {
        MouseController.OnMouseStartedEvent -= StartArrow;
        MouseController.OnMouseEndedEvent -= EndArrow;
        MouseController.OnMouseDraggingEvent -= UpdateArrow;
        TouchController.OnTouchStartedEvent -= StartArrow;
        TouchController.OnTouchEndedEvent -= EndArrow;
        TouchController.OnTouchDraggingEvent -= UpdateArrow;
    }

    void Reset()
    {
        _rigidbody.velocity = Vector2.zero;
        _transform.position = startPosition;
        _sprite.color = ReadyToTapColor;
    }

    void StartArrow(Vector2 position)
    {
        if (_collider.OverlapPoint(position) && _rigidbody.velocity == Vector2.zero)
        {
            GameObject go = SimplePool.Spawn(ArrowPrefab, position, Quaternion.identity);
            arrow = go.GetComponent<LineRenderer>();
            arrow.SetPosition(0, _transform.position);
            arrow.SetPosition(1, position);
        }
    }

    void UpdateArrow(Vector2 position)
    {
        if (arrow != null && _rigidbody.velocity == Vector2.zero)
        {
            arrow.SetPosition(0, _transform.position);
            arrow.SetPosition(1, position);
        }
    }

    void EndArrow(Vector2 position)
    {
        if (arrow != null && _rigidbody.velocity == Vector2.zero)
        {
            SimplePool.Despawn(arrow.gameObject);
            Run(position);
            arrow = null;
        }
    }

    void Run(Vector2 position)
    {
        StopCoroutine(Friction());
        _rigidbody.velocity = Vector2.zero;
        _sprite.color = Color.white;

        direction = position - (Vector2)_transform.position;
        _rigidbody.velocity = direction * Speed;
        StartCoroutine(Friction());
    }

    IEnumerator Friction()
    {
        float counter = 0;
        while (_rigidbody.velocity != Vector2.zero)
        {
            yield return new WaitForSeconds(0.2f);
            _rigidbody.velocity *= (0.9f - counter);
            counter += 0.05f;
        }
        _sprite.color = ReadyToTapColor;

        yield return null;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Obstacle"))
        {
            MetObstacle();
        }
        else if (other.gameObject.CompareTag("Gate"))
        {
            Goal();
        }
    }

    void MetObstacle()
    {
        _rigidbody.velocity = Vector2.zero;
        _rigidbody.isKinematic = true;
        OnMetObstacleEvent?.Invoke();
        UnSubscribe();
    }

    void Goal()
    {
        _rigidbody.velocity = Vector2.zero;
        float waitTime = 1f;
        OnGoalEvent?.Invoke(waitTime);
        Invoke("Reset", waitTime);
    }
}
