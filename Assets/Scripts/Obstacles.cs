﻿using UnityEngine;

public class Obstacles : MonoBehaviour
{
    [SerializeField] GameObject ObstaclePrefab;
    [SerializeField] SpriteRenderer FieldSprite;

    Transform _transform;

    void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    void Start()
    {
        SimplePool.Preload(ObstaclePrefab, 10);
        UIController.OnNewLevelEvent += OnGoalEvent;
    }

    void OnDestroy()
    {
        UIController.OnNewLevelEvent -= OnGoalEvent;
    }

    void OnGoalEvent(int level)
    {
        for (int i = 0; i < _transform.childCount; i++)
        {
            SimplePool.Despawn(_transform.GetChild(i).gameObject);
        }

        float limitX = FieldSprite.size.x * FieldSprite.transform.localScale.x / 2 - 0.5f;
        float limitY = FieldSprite.size.y * FieldSprite.transform.localScale.y / 2 - 1f;
        for (int i = 0; i < level; i++)
        {
            Spawn(new Vector2(Random.Range(-limitX, limitX),
                Random.Range(-limitY/2, limitY)));
        }
    }

    void Spawn(Vector2 position)
    {
        GameObject go = SimplePool.Spawn(ObstaclePrefab, position, Quaternion.identity);
        go.transform.parent = _transform;
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.magenta;
        float limitX = FieldSprite.size.x * FieldSprite.transform.localScale.x/2 - 0.5f;
        float limitY = FieldSprite.size.y * FieldSprite.transform.localScale.y/2 - 1f;
        Gizmos.DrawLine(new Vector3(-limitX, limitY, 0),
            new Vector3(limitX, limitY, 0));
        Gizmos.DrawLine(new Vector3(-limitX, -limitY/2, 0),
            new Vector3(limitX, -limitY/2, 0));
        Gizmos.DrawLine(new Vector3(limitX, limitY, 0),
            new Vector3(limitX, -limitY/2, 0));
        Gizmos.DrawLine(new Vector3(-limitX, limitY, 0),
            new Vector3(-limitX, -limitY/2, 0));
    }
}
