using System;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    public static event Action<Vector2> OnMouseStartedEvent;
    public static event Action<Vector2> OnMouseEndedEvent;
    public static event Action<Vector2> OnMouseDraggingEvent;

    Camera _camera;

    Vector2 mouseStartedPosition;

    void Awake()
    {
        _camera = Camera.main;
    }

	void Start()
	{
#if !UNITY_EDITOR
		Destroy(this);
#endif
	}
	
	void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
            OnMouseStartedEvent?.Invoke(
                _camera.ScreenToWorldPoint(Input.mousePosition));
		}
        else if (Input.GetMouseButtonUp(0))
        {
            OnMouseEndedEvent?.Invoke(
                _camera.ScreenToWorldPoint(Input.mousePosition));
        }
        else if (Input.GetMouseButton(0))
        {
            OnMouseDraggingEvent?.Invoke(
                _camera.ScreenToWorldPoint(Input.mousePosition));
        }
    }
}
