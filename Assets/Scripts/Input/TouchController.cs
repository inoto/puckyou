using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class TouchController : MonoBehaviour
{
	public static event Action<Vector2> OnTouchStartedEvent;
    public static event Action<Vector2> OnTouchEndedEvent;
    public static event Action<Vector2> OnTouchDraggingEvent;

    Camera _camera;

	Touch touch;

    void Awake()
    {
        _camera = Camera.main;
    }

	void Start()
	{
#if UNITY_EDITOR
		Destroy(this);
#endif
	}
	
	void Update()
	{
		if (Input.touchCount > 0)
		{
			touch = Input.GetTouch(0);
            if (touch.phase == TouchPhase.Began)
            {
                OnTouchStartedEvent?.Invoke(
                    _camera.ScreenToWorldPoint(touch.position));
            }
            if (touch.phase == TouchPhase.Stationary || touch.phase == TouchPhase.Moved)
            {
                OnTouchDraggingEvent?.Invoke(
                    _camera.ScreenToWorldPoint(touch.position));
            }
            if (touch.phase == TouchPhase.Ended)
            {
                OnTouchEndedEvent?.Invoke(
                    _camera.ScreenToWorldPoint(touch.position));
            }
        }
    }
}
