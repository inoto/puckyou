﻿using GameAnalyticsSDK;
using UnityEngine;

public class GAController : MonoBehaviour
{
    void Awake()
    {
        GameAnalytics.Initialize();
    }

    void Start()
    {
        UIController.OnNewLevelEvent += OnNewLevelEvent;
    }

    void OnDestroy()
    {
        UIController.OnNewLevelEvent -= OnNewLevelEvent;
    }

    void OnNewLevelEvent(int level)
    {
        GameAnalytics.NewDesignEvent("dtabakov:level", level-1);
    }
}
