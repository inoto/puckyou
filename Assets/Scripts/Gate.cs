using System;
using UnityEngine;
using Random = UnityEngine.Random;

public class Gate : MonoBehaviour
{
    [SerializeField] SpriteRenderer Field;
    [SerializeField] SpriteRenderer GateSprite;

    Transform _transform;

    void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    void Start()
    {
        Ball.OnGoalEvent += OnGoalEvent;
    }

    void OnDestroy()
    {
        Ball.OnGoalEvent -= OnGoalEvent;
    }

    void OnGoalEvent(float waitTime)
    {
        Invoke("ChangePosition", waitTime);
    }

    void ChangePosition()
    {
        _transform.position = new Vector3(0, _transform.position.y, _transform.position.z);

        float halfField = Field.size.x * Field.transform.localScale.x / 2;
        float halfGateSprite = GateSprite.size.x * Field.transform.localScale.x
                               * GateSprite.transform.localScale.x / 2;
        float limitX = halfField - halfGateSprite;
        float offset = Random.Range(-limitX, limitX);
        _transform.Translate(offset, 0, 0);
    }

    void OnDrawGizmos()
    {
        float halfField = Field.size.x * Field.transform.localScale.x / 2;
        float halfGateSprite = GateSprite.size.x * Field.transform.localScale.x 
                               * GateSprite.transform.localScale.x / 2;
        float limitX = halfField - halfGateSprite;
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position,
            new Vector3(-limitX, transform.position.y, 0));
        Gizmos.DrawLine(transform.position,
            new Vector3(limitX, transform.position.y, 0));
    }
}
