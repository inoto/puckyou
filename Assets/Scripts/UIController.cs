using System;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIController : MonoBehaviour
{
    public static event Action<int> OnNewLevelEvent; 

    [SerializeField] TextMeshProUGUI LevelText;
    [SerializeField] GameObject GameOverObj;
    [SerializeField] GameObject GoalTextObj;

    int level;

    void Start()
    {
        level = 1;
        UpdateLevelText();
        Ball.OnGoalEvent += OnGoal;
        Ball.OnMetObstacleEvent += OnMetObstacleEvent;
    }

    void OnDestroy()
    {
        Ball.OnGoalEvent -= OnGoal;
        Ball.OnMetObstacleEvent -= OnMetObstacleEvent;
    }

	void OnGoal(float waitTime)
    {
        GoalTextObj.SetActive(true);
        Invoke("HideGoalText", waitTime);
	}

    void UpdateLevelText()
    {
        LevelText.text = $"LEVEL {level}";
    }

    void HideGoalText()
    {
        level++;
        UpdateLevelText();
        OnNewLevelEvent?.Invoke(level);
        GoalTextObj.SetActive(false);
    }

    void OnMetObstacleEvent()
    {
        GameOverObj.SetActive(true);
        Invoke("RestartGame", 3f);
    }

    void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
